#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from a6lte device
$(call inherit-product, device/samsung/a6lte/device.mk)

PRODUCT_DEVICE := a6lte
PRODUCT_NAME := omni_a6lte
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-A600FN
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung-ss

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="a6ltexx-user 10 QP1A.190711.020 A600FNXXU9CVB1 release-keys"

BUILD_FINGERPRINT := samsung/a6ltexx/a6lte:10/QP1A.190711.020/A600FNXXU9CVB1:user/release-keys
